

//sem by som daval globalne premenne ktore potrebuju vsekty suciastky

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

typedef enum Errors
{
   	E_LEX = 1,		// chyba v programu v rámci lexikální analýzy
  	E_SYNT = 2,		// chyba v programu v rámci syntaktické analýzy
   	E_SEM_PROG = 3,		// sémantická chyba v programu – nedefinovaná trída/funkce/promenná, pokus o re-definici trídy/funkce/promenné
	E_SEM_KO = 4,		// sémantická chyba typové kompatibility v aritmetických, retezcových a relacních výrazech, príp. špatný pocetci typ parametr  °u u volání funkce.
	E_SEM_INE = 6,		// ostatní sémantické chyby
	E_BEH_CIS = 7,		// behová chyba pri nacítání císelné hodnoty ze vstupu
	E_BEH_NINIT = 8,	// behová chyba pri práci s neinicializovanou promennou
 	E_BEH_ZDIV = 9,		// behová chyba delení nulou
	E_BEH_INE = 10,		// ostatní behové chyby
	E_INTERN = 99		// interní chyba interpretu
} tErrors;


//Globalne premenne
FILE *subor;
tErrors error;
