#ifndef INCLUDED_H_ILIST
#define INCLUDED_H_ILIST

#include <stdio.h>
#include <stdlib.h>

typedef enum {
// aritmetické
	iADD, // 0  sčítání
	iSUB, // 1  odčítání
	iMUL, // 2	násobení
	iDIV, // 3	dělení

// relační
	iGT, //  4	větší než
	iLT, //  5	menší než
 	iEQ, //  6	rovno
  iNEQ, // 7	nerovno
  iGTE, // 8	větší nebo rovno
  iLTE, // 9	menší nebo rovno

// systémové
	iOUT, // 10	výstup textu
	iIN, //  11	vstup textu
	iASS, // 12	přiřazení
	iRET, // 13	návrat z funkce
  iCON, // 14	převod dat. typů
	iCALL, //15 volání funkcí
	iPUSH, //16 vložení na zásobník
	iPOP, // 17 vytáhnutí ze zásobníku
} iType;

typedef struct {
	iType type;
	void *ptr1;
	void *ptr2;
	void *ptr3;
} Instruction;

typedef struct iLItem {
	struct iLItem *next;
	Instruction instruction;
} *pILItem;

typedef struct {
	pILItem first;
	pILItem active;
	pILItem last;
} iList;

void iListInit(iList*);
void iListDestruct(iList*);
int iListEmpty(iList*);
int iListInsertAfter(iList*, Instruction);
int iListInsertFirst(iList*, Instruction);
int iListInsertLast(iList*, Instruction);
void iListRemove(iList*);
void iListRemoveFirst(iList*);
void iListRemoveLast(iList*);
void iListFirst(iList*);
void iListLast(iList*);
void iListSucc(iList*);
void iListPred(iList*);
Instruction *iListGet(iList*);
Instruction *iListGetFirst(iList*);
Instruction *iListGetLast(iList*);

void iListPrint(iList*);

#endif
