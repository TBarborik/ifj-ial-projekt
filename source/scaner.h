//hlavicka pro lexikalni analyzator

typedef enum 
{
    	S_START,        // 00 - pociatocny stav
    	S_KLUC,         // 01 - klucove slovo
    	S_ENDOFFILE,    // 02 - EOF
    	S_KONIEC,       // 03 - koncovy stav
	S_IDENT,        // 04 - indentifikator
	S_INT,          // 05 - integer
   	S_FLOAT,	// 06 - float
    	S_EXPO,         // 07 - exponential
    	S_STR,          // 08 - retazec
    	S_PRIRAD,       // 09 - =
    	S_STREDNIK,     // 10 - ;
    	S_CIARKA,       // 11 - ,
    	S_BODKA,        // 12 - .
    	S_LZ,           // 13 - (
    	S_PZ,           // 14 - )
    	S_ADD,          // 15 - +
    	S_SUB,          // 16 - -
    	S_MUL,          // 17 - *
    	S_DIV,          // 18 - /
    	S_VEC,          // 19 - >
    	S_MEN,          // 20 - <
    	S_POROV,        // 21 - ==
    	S_NEROV,        // 22 - !=
    	S_VECROV,       // 23 - >=
    	S_MENROV,       // 24 - <=
	S_LINEKOMENT,	// 25 - //
	S_MULTIKOMENT,	// 26 - /* (.*) */
	S_ERROR,	// 27
	S_FLOATPREP,	// 28 
	S_EXPOPREPSD,	// 29
	S_EXPOPREP,	// 30
	S_STRBS,	// 31
	S_LZZ,		// 32
	S_PZZ		// 33
} tStav;


typedef struct
{
    tStav stav;
    char *value;
} tToken;


tToken getToken(void);  // vrati token
