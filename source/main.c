/* Hlavicka treba doplnit */

#include "globalvariables.h"
#include "scaner.h"
#include "ilist.h"

int main (int argc, char **argv) {
	int result = 0;

	//overenie ci dostavame len jeden agument
	if (argc != 2) {
		fprintf(stderr, "Zle zadan� argumenty programu!\n");
		return E_INTERN; //chybovy kod intenej chyby
	}
	
	//overenie ci sa subor da otvorit
	if (!(subor = fopen(argv[1], "r") ) ) {
		fprintf(stderr, "Zadany suboer sa nepodarilo otvorit!");
		return E_INTERN; //chybovy kod intenej chyby
	}
	
	//parser
	tToken token;
	while((token = getToken()).stav != S_ENDOFFILE) {
		printf("%s : %d\n", token.value, token.stav);
	}
	//parser
	
	if (result) {
		return result;
	}
	return 0;
}
