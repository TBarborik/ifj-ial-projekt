#include "ilist.h"

/**
 * Inicializuje seznam instrukcí
**/ 
void iListInit(iList *list)
{
	list->active = list->first = list->last = NULL;	
}

/**
 * Odstraní a uvolní všechny prvky ze seznamu instrukcí
**/
void iListDestruct(iList *list)
{
	pILItem item = NULL;
	while (list->first != NULL) {
		item = list->first;
		list->first = item->next;
		free(item);
	}
	
	list->active = list->last = list->first;
}

/**
 * Kontroluje, zda je seznam prázdný
 * @return Je-li seznam prázdný, vrací true, jinak false
**/
int iListEmpty(iList *list)
{
	return list->first == NULL;
}


/**
 * Vkládá nový prvek za aktivní
 * @return Při podařeném vložení vrací true, jinak false
**/
int iListInsertAfter(iList *list, Instruction ins)
{
	pILItem item = malloc(sizeof(struct iLItem));
	if (item != NULL) {
		item->next = list->active->next;
		item->instruction = ins;

		if (list->active == list->last) {
			list->last = item;
		}

		list->active->next = item;
		return 1;
	}		

	return 0;
}

/**
 * Vkládá nový prvek jako první
 * @return Při podařeném vložení vrací true, jinak false
**/
int iListInsertFirst(iList *list, Instruction ins)
{
	pILItem item = malloc(sizeof(struct iLItem));
	if (item != NULL) {
		item->next = list->first;
		item->instruction = ins;

		if (iListEmpty(list)) {
			list->last = item;
		}

		list->first = item;
		return 1;
	}

	return 0;
}

/**
 * Vkládá nový prvek jako poslední
 * @return Při podařeném vložení vrací true, jinak false
**/
int iListInsertLast(iList *list, Instruction ins)
{
	pILItem item = malloc(sizeof(struct iLItem));
	if (item != NULL) {
		item->next = NULL;
		item->instruction = ins;

		if (iListEmpty(list)) {
			list->first = item;
		} else {
			list->last->next = item;
		}

		list->last = item;
		return 1;
	}

	return 0;
}

/**
 * Odstraňuje aktivní prvek
**/
void iListRemoveActive(iList *list)
{
	if (!iListEmpty(list)) {
		if (list->active == list->first) { // Aktivní je první = odstraní první a nastaví nový první jako aktivní
			iListRemoveFirst(list);
			list->active = list->first;
		} else { // AKtivní je poslední nebo někde mezi 
			pILItem item = list->first;
			pILItem back = NULL;
		
			while (item != list->active) {
				back = item;
				item = item->next;
			}	

			back->next = item->next;
			free(item);
		}
	}
}

/**
 * Odstraní první prvek ze seznamu
**/
void iListRemoveFirst(iList *list)
{
	if (!iListEmpty(list)) {
		pILItem item = list->first;
		list->first = item->next;

		if (item == list->last) {
			list->last = NULL;		
			list->active = NULL;
		}
		
		free(item);
	}
}

/**
 * Odstraní poslední prvek ze seznamu
**/
void iListRemoveLast(iList *list)
{
	if (!iListEmpty(list)) {
		pILItem item = list->active;
		list->active = list->last;
		iListRemoveActive(list);
	
		if (iListEmpty(list))
			list->active = NULL;	
		else
			list->active = item;
	}
}

/**
 * Nastaví první jako aktivní
**/
void iListFirst(iList *list)
{
	list->active = list->first;
}

/**
 * Nastaví poslední jako aktivní
**/
void iListLast(iList *list)
{
	list->active = list->last;
}

/**
 * Posune aktivní na další prvek, pokud je to možné
**/
void iListSucc(iList *list)
{
	if (list->active != NULL) {
		list->active = list->active->next;
	}
}

/**
 * Posune aktivní na předcházející, je-li to možné
**/
void iListPred(iList *list)
{
	if (list->active != list->first) {
		list->active = list->first;
		pILItem item = list->active;

		while (list->active->next != item) {
			list->active = list->active->next;
		}
	}
}

/**
 * Vrací ukazatel na instrukci z aktivní položky
 * @return Vrací ukazatel na instrukci nebo NULL
**/
Instruction *IListGet(iList *list) 
{
	if (list->active != NULL)
		return &list->active->instruction;
	
	return NULL;
}

/**
 * Vrací ukazate na instrukci z první položky
 * @return Vrací ukazatel na instrukci nebo NULL
**/
Instruction *IListGetFirst(iList *list)
{
	if (!iListEmpty(list))
		return &list->first->instruction;

	return NULL;
}

/**
 * Vrací ukazate na instrukci z poslední položky
 * @return Vrací ukazatel na instrukci nebo NULL
**/
Instruction *IListGetLast(iList *list)
{
	if (!iListEmpty(list))
		return &list->last->instruction;

	return NULL;
}

/**
 * Vytiskne prvky seznamu na jednotlivé řádky
**/
void iListPrint(iList *list) 
{
	printf("----------[ZACATEK TABULKY]----------\n");
	printf("%3s | %3s | %3s | %10s | %10s | %10s | %10s\n", "Pos", "Prv", "Akt", "Instrukce", "1. odkaz", "2. odkaz", "3. odkaz");
	pILItem item = list->first;
	for (; item != NULL; item = item->next) {
		printf("%3s | %3s | %3s | %10d | %10p | %10p | %10p\n", list->last == item ? "-" : "", list->first == item ? "-" : "", list->active == item ? "-" : "", item->instruction.type, item->instruction.ptr1, item->instruction.ptr2, item->instruction.ptr3);
	}
	printf("-----------[KONEC TABULKY]------------\n");
}
