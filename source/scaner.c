// lexikalni analyzator
#include "scaner.h"
#include "globalvariables.h"

#define PocetKlucovychSlov 17

tToken token;

const char* klucSlova[PocetKlucovychSlov] = {
	"boolean\0", "break\0", "class\0", "continue\0", "do\0", "double\0", "else\0", "false\0", "for\0", "if\0", "int\0", "return\0", "String\0", "static\0", "true\0", "void\0", "while\0"
};

void vratChar( int character )
{	
	ungetc(character, subor);
}

int alokujAPridajZnak ( int character, int * velkost ) {
	if (token.value == NULL) {
		if ((token.value = malloc(*velkost + 2)) == NULL){
			return 1;
		}
	} else {
		if ((token.value = (char *) realloc(token.value, (*velkost + 2))) == NULL) {
			return 1;
		}
	}
	
	token.value[(*velkost) + 1] = '\0';
  	token.value[(*velkost)] = character;
	(*velkost)++;
	return 0;
}

tStav prejdiKlucoveSlova(char *slovo)
{
    int i;
    // prejdeme klucove slova
    for (i = 0; i < PocetKlucovychSlov; i++)
        if (!(strcmp(slovo, klucSlova[i]) ) ){
            return S_KLUC;
		}	

    return S_IDENT;
}

void inicToken()
{
    token.stav = S_START;
    token.value = NULL;
}

tToken getToken(){
	inicToken();

	int c;
	
	tStav stav = S_START;
	int lock = 1;
	
	int znakovaDlzkaTokenu = 0;
	
	while(lock && (c = getc(subor))) {
		switch (stav) {
			case S_START: {
				if ((isalpha(c)) || c == ('_') || c == ('$')) stav = S_IDENT;
				else if (isdigit(c)) stav = S_INT;
				else if (c == '/') stav = S_DIV;
				else if (c == '"') stav = S_STR;
				else if (c == '>') stav = S_VEC;
				else if (c == '=') stav = S_PRIRAD;
				else if (c == '*') stav = S_MUL;
				else if (c == '<') stav = S_MEN;
				else if (c == '-') stav = S_SUB;
				else if (c == '+') stav = S_ADD;
				else if (c == '!') stav = S_NEROV;
				else if (c == '(') stav = S_LZ;
				else if (c == ')') stav = S_PZ;
				else if (c == '{') stav = S_LZZ;
				else if (c == '}') stav = S_PZZ;
				else if (c == ';') stav = S_STREDNIK;
				else if (c == ',') stav = S_CIARKA;
				else if (c == '.') stav = S_BODKA;
				else if (c == EOF) {
					stav = S_ENDOFFILE;
					continue;
				}
				else if (isspace(c)) {  break; }
				else {
					stav = S_ERROR;
				}
				
				if (c != '"') {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
				}
				break;
			}
			
			case S_IDENT: {
				if ((isalnum(c)) || c == ('_') || c == ('$')) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
				} else {
					token.stav = prejdiKlucoveSlova(token.value);
					vratChar(c);
					stav = S_KONIEC;
				}
				
				break;
			}

			case S_INT: {
				if (isdigit(c)) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
				} else if (c == '.') {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_FLOATPREP;
				} else if ((c == 'E') || (c == 'e')){
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_EXPOPREPSD;
				} else if (isspace(c) || (c == '+') || (c == '-') || (c == '*') || (c == '/') || (c == '(') || (c == ')')) {
					token.stav = S_INT;
					vratChar(c);
					stav = S_KONIEC;
				} else {
					stav = S_ERROR;
				}
				break;
			}

			case S_FLOAT: {
				if (isdigit(c)) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
				} else if ((c == 'E') || (c == 'e')){
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_EXPOPREPSD;
				} else if (isspace(c) || (c == '+') || (c == '-') || (c == '*') || (c == '/') || (c == '(') || (c == ')')) {
					token.stav = S_FLOAT;
					vratChar(c);
					stav = S_KONIEC;
				} else {
					stav = S_ERROR;
				}
				break;
			}

			case S_FLOATPREP: {
				if (isdigit(c)) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_FLOAT;
				} else {
					stav = S_ERROR;
				}
				break;
			}

			case S_EXPOPREPSD: {
				if ((c == '+') || (c == '-')) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_EXPOPREP;
				} else if (isdigit(c)) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_EXPO;
				} else {
					stav = S_ERROR;
				}
				break;
			}

			case S_EXPOPREP: {
				if (isdigit(c)) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_EXPO;
				} else {
					stav = S_ERROR;
				}
				break;
			}

			case S_EXPO: {
				if (isdigit(c)) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
				} else if (isspace(c) || (c == '+') || (c == '-') || (c == '*') || (c == '/') || (c == '(') || (c == ')')) {
					token.stav = S_EXPO;
					vratChar(c);
					stav = S_KONIEC;
				} else {
					stav = S_ERROR;
				}
				break;
			}

			case S_STR: {
				if (c == '"') {
					token.stav = S_STR;
					stav = S_KONIEC;
				} else if (c == 92) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_STRBS;
				} else if (c > 31) {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
				}
				break;
			}

			case S_STRBS: {
				if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
					token.stav = S_ERROR;
					error = E_INTERN;
					return token;
				}
				stav = S_STR;
				break;
			}

			case S_PRIRAD: {
				if (c == '=') {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_POROV;
				} else {
					token.stav = S_PRIRAD;
					stav = S_KONIEC;
				}
				break;
			}

			case S_VEC: {
				if (c == '=') {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_VECROV;
				} else {
					token.stav = S_VEC;
					stav = S_KONIEC;
				}
				break;
			}

			case S_MEN: {
				if (c == '=') {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					stav = S_MENROV;
				} else {
					token.stav = S_MEN;
					stav = S_KONIEC;
				}
				break;
			}

			case S_NEROV: {
				if (c == '=') {
					if (alokujAPridajZnak(c, &znakovaDlzkaTokenu)) {
						token.stav = S_ERROR;
						error = E_INTERN;
						return token;
					}
					token.stav = S_NEROV;
					stav = S_KONIEC;
				} else {
					stav = S_ERROR;
				}
				break;
			}

			case S_DIV: {
				if (c == '/') {
					stav = S_LINEKOMENT;
				} else if (c == '*') {
					stav = S_MULTIKOMENT;
				} else {
					token.stav = S_DIV;
					stav = S_KONIEC;
				}	
				break;						
			}

			case S_LINEKOMENT: {
				if (c == '\n') {
					token.stav = S_LINEKOMENT;
					stav = S_KONIEC;
				} else if (c == EOF) {
					token.stav = S_LINEKOMENT;
					stav = S_KONIEC;
					continue;
				}
				break;
			}

			case S_MULTIKOMENT: {
				if (c == '*') {
					c = getc(subor);
					if (c == '/') {
						token.stav = S_MULTIKOMENT;
						stav = S_KONIEC;
					} else if (c == EOF) {
						token.stav = S_LINEKOMENT;
						stav = S_KONIEC;
						continue;
					}
				} else if (c == EOF) {
					token.stav = S_LINEKOMENT;
					stav = S_KONIEC;
					continue;
				}
				break;
			}

			case S_MUL:
			case S_ADD:
			case S_SUB:
			case S_LZ:
			case S_PZ:
			case S_LZZ:
			case S_PZZ:
			case S_STREDNIK:
			case S_CIARKA:
			case S_BODKA:
			case S_KLUC:
			case S_POROV:
			case S_VECROV:
			case S_MENROV:
			{
				vratChar(c);
				token.stav = stav;
				stav = S_KONIEC;
				break;
			}

			case S_ENDOFFILE: {
				fclose(subor);
				token.stav = S_ENDOFFILE;
				return token;
			}

			case S_ERROR: {
				token.stav = S_ERROR;
				error = E_LEX;
				vratChar(c);
				return token;
			}
			
			case S_KONIEC: {
				vratChar(c);
				lock = 0;
				break;
			}
		}
		
	}
	return token;
}
